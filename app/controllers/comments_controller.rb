class CommentsController < ApplicationController

  before_filter :authenticate_user!, except: [:index, :show]

  def show
    @comment = Comment.find params[:id]
  end


  def create

    comment = Comment.new(comment_params)

    if comment.save
      # A store for ratings
      score = []
      # Find all ratings
      comment.item.comments.each do |c|
        if c.rating
          score << c.rating
        end
      end
      # If there are some
      if score.any? # any? works for massives and hashes
        # Find average
        sum = score.sum.to_f
        avg = sum / score.count

        # Add it to item
        comment.item.update({ rating: avg })
      end

      redirect_to comment.item, notice: 'Comment was successfully created.'
    else
      redirect_to comment.item, alert: 'Comment is not created'
    end

  end



  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params[:comment].permit(:name, :item_id, :message, :rating)
    end
end
