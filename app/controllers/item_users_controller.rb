class ItemUsersController < ApplicationController

  before_filter :authenticate_user!

  # GET /item_users
  # GET /item_users.json
  def index
    @item_users = current_user.item_users
  end

  # POST /item_users
  # POST /item_users.json
  def create
    item = Item.find(params[:item_user][:item_id])

    item_user = ItemUser.where(user_id: current_user.id).where(item_id: item.id).first

    if item_user
      item_user.quantity += 1
      item_user.save
    else
      item_user = ItemUser.new
      item_user.item_id   = item.id
      item_user.user_id   = current_user.id
      item_user.quantity  = 1
      item_user.save
    end

    redirect_to :back
  end

  def update
    # Find
    item = ItemUser.find(params[:id])
    # Check Permissions
    if item.user_id == current_user.id
      # Update
      item.update(quantity: params[:item_user][:quantity])
    end
    # Redirect_to cart
    redirect_to item_users_path
  end


  # DELETE /item_users/1
  # DELETE /item_users/1.json
  def destroy
    item = ItemUser.find(params[:id])
    item.destroy if item.user_id == current_user.id
    redirect_to item_users_path
  end

end
