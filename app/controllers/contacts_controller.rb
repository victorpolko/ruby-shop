class ContactsController < ApplicationController
  layout  'application'

  before_filter :authenticate_user!, except: [:index, :show]

  def index
    @contacts = Contact.order(name: :asc)
  end

  def new
    @contact  = Contact.new # @ means that this variable is seen inside the VIEW file
    b = 1           # this variable's scope is this DEF only
  end

  def create
    @contact  = Contact.new(params[:contact].permit(:name,:phone,:email,:website,:address)) # params is all data from user; params[:{modelName}] filters them by model; permit gives permission to record data for named fields
    if  @contact.save
      redirect_to contacts_path
    else
      render 'new'
    end
  end

  def edit
    # Find a contact by id
    # params is an object that is got from web
    @contact  = Contact.find(params[:id])
  end

  def update
    # Find a record
    @contact  = Contact.find(params[:id])

    # Update props
    if @contact.update params[:contact].permit(:name,:phone,:email,:website,:address)
      # Goto catlist
      redirect_to contacts_path
    else
      render 'edit'
    end
  end

  def destroy
    # Find a record
    @contact  = Contact.find(params[:id])
    # Destroy record
    @contact.destroy
    # Goto catlist
    redirect_to contacts_path
  end

  def show
    @contact  = Contact.find(params[:id])
  end
end
