class MembersController < ApplicationController
  layout  'application'

  before_filter :authenticate_user!, except: [:index, :show]

  def index
    @members  = Member.order(name: :asc)
  end

  def new
    @member = Member.new
  end

  def create
    @member = Member.new(params[:member].permit(:name,:position,:age))
    if  @member.save
      redirect_to members_path
    else
      render 'new'
    end
  end

  def edit
    # Find a category by id
    # params is an object that is got from web
    @member = Member.find(params[:id])
  end

  def update
    # Find a record
    @member = Member.find(params[:id])

    # Update props
    if @member.update_attributes params[:member].permit(:name,:position,:age)
      # Goto catlist
      redirect_to members_path
    else
      render 'edit'
    end
  end

  def destroy
    # Find a record
    @member = Member.find(params[:id])
    # Destroy record
    @member.destroy
    # Goto catlist
    redirect_to members_path
  end

  def show
    @member = Member.find(params[:id])
  end
end
