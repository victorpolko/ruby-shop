class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :define_language

  def define_language

    lang = params[:lang]
    # If language was not set manually
    if lang.blank?
      lang = cookies[:lang]
      # If user has no saved language
      if lang.blank?
        # If it is no possible to define language with browser
        unless request.headers['HTTP_ACCEPT_LANGUAGE'].blank?
          # Set to default
          lang = request.headers['HTTP_ACCEPT_LANGUAGE'][0..1]
          unless %w[ru en].include? lang
            lang = nil
          end
        end
      end
    end

    unless lang.blank?
      I18n.locale = lang
      cookies[:lang] = { value: lang, expires_in: 3.months }
    end

    I18n.locale = params[:lang] unless params[:lang].blank? && !%w[ru en].include?(params[:lang])
  end
end
