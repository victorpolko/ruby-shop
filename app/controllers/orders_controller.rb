class OrdersController < ApplicationController
  before_action :authenticate_user!

  def index
    @orders = current_user.orders
  end

  def create
    # No items in cart -> redirect to somewhere else
    unless current_user.item_users.any?
      redirect_to root_path
    end
    # Create the order
    order = Order.create! user_id: current_user.id,
                          price: ItemUser.total_price(current_user),
                          status: 'created'
    # Move items from cart to order
    current_user.item_users.each do |i|
      ItemOrder.create! quantity: i.quantity,
                        order_id: order.id,
                        item_id: i.item_id, # could be i.item.id but this is a new SQL request
                        price: i.item.price
    end
    # Clear the cart
    current_user.item_users.destroy_all
    # Send letters
    OrderMailer.order_created_for_user(order)
    OrderMailer.order_created_for_admin(order)
    # Go to order show
    redirect_to order
  end

  def show
    @order = Order.find params[:id]
    unless @order.user_id == current_user.id
      raise 'Order not found', code: 404
    end
  end
end
