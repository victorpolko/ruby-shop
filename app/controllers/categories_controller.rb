class CategoriesController < ApplicationController

  before_filter :authenticate_user!, except: [:index, :show]

  layout  'application'

  def index
    @categories = Category.order(name: :asc)
  end

  def new
    @category = Category.new  # @ means that this variable is seen inside the VIEW file
    b = 1           # this variable's scope is this DEF only
  end

  def create
    @category = Category.new(params[:category].permit(:name)) # params is all data from user; params[:{modelName}] filters them by model; permit gives permission to record data for named fields
    if  @category.save
      redirect_to categories_path
    else
      render 'new'
    end
  end

  def edit
    # Find a category by id
    # params is an object that is got from web
    @category = Category.find(params[:id])
  end

  def update
    # Find a record
    @category = Category.find(params[:id])

    # Update props
    if @category.update_attributes params[:category].permit(:name)
      # Goto catlist
      redirect_to categories_path
    else
      render 'edit'
    end
  end

  def destroy
    # Find a record
    @category = Category.find(params[:id])
    # Destroy record
    @category.destroy
    # Goto catlist
    redirect_to categories_path
  end

  def show
    @cat  = Category.find(params[:id])
    @all  = Category.order(name: :asc)
  end

end
