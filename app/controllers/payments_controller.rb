class PaymentsController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def cancel
    #
  end

  def notify
    #
  end

  def success
    # Filter params from Web
    filtered_params = params
    filtered_params.delete(:action)
    filtered_params.delete(:controller)
    filtered_params[:cmd] = '_notify-validate'

    # Check if these are PayPal's ones
    response = Faraday.post 'https://www.paypal.com/cgi-bin/webscr', filtered_params
    response_body = response.body

    # If body = verified
    if response_body = 'VERIFIED'
      # If needs to be refunded to payer
      if params[:payments_status].to_s == 'Refunded'
        render text:'OK'
        # Things to be done after refunding (change order status etc.)
        return
      end
      #
      unless params[:receiver_email] == "victorpolko@gmail.com"
        raise "receiver_email was #{ params[:receiver_email] } needed victorpolko@gmail.com"
        return
      end

      #
      unless params[:txn_type] == "web_accept"
        raise "txn_type was #{ params[:txn_type] } needed web_accept"
        return
      end

      # Find order
      order = Order.find params[:item_number]

      # Check sum
      unless order.price >= params[:mc_gross].to_f || params[:mc_currency] == 'RUB'
        raise "mc_gross was #{ params[:mc_gross].to_f } needed #{ order.price } or mc_currency was #{ params[:mc_currency] } needed RUB"
        return
      end

      # Order payed?
      if order.status == 'paid'
        render text:'OK'
        return
      end

      order.update status: 'paid'

      # @todo Send emails about payment

    end

    render text:'OK'

  end
end
