class Item < ActiveRecord::Base
# Associations
  belongs_to  :category
  has_many    :comments, dependent: :destroy
  has_many    :item_users, dependent: :destroy
  has_many    :item_orders, dependent: :destroy

# Validations
  validates :name, :price, presence: true
  validates :name, uniqueness: true
  validates :price, numericality: {
    greater_than_or_equal_to: 0.01
  }
  validates :name, length: {
    minimum:  2
  }

#
  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100#" }, default_url: "/system/items/photos/:style/missing.png"

  def ratingBadge
    if rating
      if rating >= 4
        'success'
      elsif rating > 2
        'warning'
      else
        'important'
      end
    end
  end
end
