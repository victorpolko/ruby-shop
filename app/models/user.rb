class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Associations
  has_many    :item_users, dependent: :destroy
  has_many    :orders, dependent: :destroy

  # Validations
  # validates

  # Custom methods
  def full_price
    self.item_users.quantity * self.item_users.item.price
  end
  def totalCartPrice
    total = 0
    self.item_users.each do |i|
      total += i.item.price * i.quantity
    end
    total
  end

  def cartQ
    total = 0
    self.item_users.each do |i|
      total += i.quantity
    end
    total
  end

end
