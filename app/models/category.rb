class Category < ActiveRecord::Base

# Associations
  has_many :items, dependent: :nullify

  validates :name, presence: true, format:  {
    with: /\A[a-zA-Z]+\z/,
    message: "Only letters allowed!"
  }, length: {
    minimum: 3
  }
end
