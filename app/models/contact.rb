class Contact < ActiveRecord::Base
  validates :name,  :phone, :email, presence: true
  validates :name,  :address, format: {
    with: /[0-9а-яА-Яa-zA-Z.,]{3,}\z/,
    message: "Wrong..."
  }
  validates :phone, format: {
    with: /\+\d{10,}\b/,
    message: "Doesn't match the format! (+7123456789)"
  }
  validates :website, format: {
    with: /\w{2,}\.\w{2,}\.\w{2,5}\b/,
    message: "Doesn't match the format! (www.example.com)"
  }
end
