class ItemOrder < ActiveRecord::Base
  # Associations
  belongs_to :order
  belongs_to :item

  # Validations
  validates :order_id, :quantity, :item_id, :price, presence: true
  validates :order_id, :item_id, :quantity, numericality: { only_integer: true, greater_than: 0 }
  validates :price, numericality: { greater_than: 0 }
end
