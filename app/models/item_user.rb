class ItemUser < ActiveRecord::Base

  # Associations
  belongs_to :item
  belongs_to :user

  # Validations
  validates :item_id, :user_id, :quantity, presence: true, numericality: {
    only_integer: true,
    greater_than: 0
  }

  # Custom methods
  def self.total_price(user)
  	total = 0.0
  	user.item_users.each do |i|
  		total += i.full_price
  	end
  	total
  end

  def full_price
    quantity * item.price
  end

end
