class OrderMailer < ActionMailer::Base
  default from: "noreply@protected-thicket-8526.herokuapp.com"

  def order_created_for_user(order)
    @order = order
    mail(to: order.user.email, subject: 'Order in our shop was made!').deliver
  end

  def order_created_for_admin(order)
    @order = order
    mail(to: "victorpolko@gmail.com", subject: 'New order was made!').deliver
  end

end
