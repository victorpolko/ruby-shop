# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.find_or_create_by name: 'Cats'
Category.find_or_create_by name: 'Birds'
Category.find_or_create_by name: 'Whales'

Item.find_or_create_by name: 'Seagull', category_id: 2, price: 10
Item.find_or_create_by name: 'Eagle', category_id: 2, price: 5
