class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string  :name
      t.string  :position
      t.string  :age
      t.timestamps
    end
  end
end
